﻿#include <iostream>

using namespace std;

template<typename T>
class Stack
{
public:
    Stack()
    {
        top = 0;
        size = 1;
        arr = new int[size];
        arr[0] = 0;
    }
    void push(T  newelement)
    {
        int* newArray = new int[size + 1];
        for (int i = 0; i < size; i++)
        {
            newArray[i] = arr[i];
        }
        newArray[size - 1] = newelement;
        size++;
        top++;
        delete[] arr;
        arr = newArray;
    }
    void pop()
    {
        if (top <= 0)
        {
            cout << "No element for pop()" << endl;
        }
        else
        {
            T Array = arr[top--];
        }
    }
    T topelement()
    {
        if (top <= 0)
        {
            cout << " No top element" << endl;
        }
        else
            return arr[top-1];
    }
    bool isEmpty()
    {
        return (top <= 0);
    }
    void capacity()
    {
        cout << " Stack size:" << top << endl;
    }
    ~Stack()
    {
        delete[] arr;
    }
private:
    int* arr;
    int size;
    int top;
   
};

int main()
{
    Stack<int> c;
    c.push(5);
    c.push(6);
    cout << " Top element of stack:" << c.topelement() << endl;
    c.capacity();
    cout << " Stack elements" << endl;
    while (!c.isEmpty())
    {
        cout << c.topelement() << " ";
        c.pop();
    }
    cout << endl;
    system("pause");
}
